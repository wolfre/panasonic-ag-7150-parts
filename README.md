# Panasonic AG-7150 Parts

A bunch of 3D printable parts for a Panasonic AG-7150 and related S-VHS decks.
The parts may also fit other models like the Panasonic AG-7350.

```
This project is in no way whatsoever associated with, or endorsed by, Panasonic.
```

Parts:
- [Backplate](backplate) - things for the 34p C.B.A. slot


## License

The parts are generally under *Creative Commons Attribution-ShareAlike 4.0*.
