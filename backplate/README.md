# Back plate

A back plate for 34p C.B.A. slot / module in multiple variants:

![no-holes.jpg](no-holes.jpg)

A clean slate version.

![bnc-holes.jpg](bnc-holes.jpg)

A version with mounting holes for 2 BNC (13 mm diameter holes).
